<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>User Page</title>
	</head>
	<body>
		<h3>List Users</h3>
		<table>
			<tr>
				<th>Username</th>
				<th>Email</th>
			</tr>
			<c:forEach var="user" items="${listUser}">
				<tr>
					<td>${user.username}</td>
					<td>${user.email}</td>
					<td><a href="${pageContext.request.contextPath}/user/edit/${user.id}">Edit</a> </td>
					<td><a href="${pageContext.request.contextPath}/user/delete/${user.id}">Delete</a> </td>
				</tr>
			</c:forEach>
		</table>
	</body>
</html>