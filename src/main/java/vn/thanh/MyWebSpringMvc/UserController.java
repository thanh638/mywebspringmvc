package vn.thanh.MyWebSpringMvc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import vn.thanh.MyWebSpringMvc.entities.User;
import vn.thanh.MyWebSpringMvc.service.UserService;

@Controller
@RequestMapping("user")
public class UserController {
	private UserService userService;
	
	@Autowired(required=true)
	@Qualifier(value="userService")
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	// get all User Controller
	@RequestMapping(value="", method=RequestMethod.GET)
	public String getAll(Model model) {
		model.addAttribute("listUser", this.userService.getAll());
		return "user";
	}
	
	// Add User and Update User Controller
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String saveUser(@ModelAttribute("User") User user) {
		if (user.getId() == 0) {
			// add
			this.userService.addItems(user);
		} else {
			// update
			this.userService.updateItem(user);
		}
		return "redirect:/user";
	}
	
	// Delete User Controller
	@RequestMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") int id) {
		this.userService.delete(id);
		return "redirect:/user";
	}
	
	// Show Edit User
	@RequestMapping(value="/edit/{id}")
	public String editUser(@PathVariable("id") int id, Model model) {
		model.addAttribute("user", this.userService.getItemById(id));
		System.out.println(this.userService.getItemById(id).getUsername());
		return "edit";
	}
	
	// show form add
	@RequestMapping("/add")
	public String addUser() {
		return "add";
	}

}
