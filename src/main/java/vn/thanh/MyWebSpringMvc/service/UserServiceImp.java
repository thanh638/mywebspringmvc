package vn.thanh.MyWebSpringMvc.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.thanh.MyWebSpringMvc.dao.UserDao;
import vn.thanh.MyWebSpringMvc.entities.User;

@Service
public class UserServiceImp implements UserService{
	private UserDao userDao;

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	@Transactional
	public List<User> getAll() {
		return this.userDao.getAll();
	}

	@Override
	@Transactional
	public void addItems(User user) {
		this.userDao.addItems(user);
		
	}

	@Override
	@Transactional
	public void updateItem(User user) {
		this.userDao.updateItem(user);
	}

	@Override
	@Transactional
	public User getItemById(int id) {
		return this.userDao.getItemById(id);
	}

	@Override
	@Transactional
	public void delete(int id) {
		this.userDao.delete(id);
	}

}
