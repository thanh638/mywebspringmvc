package vn.thanh.MyWebSpringMvc.service;

import java.util.List;

import vn.thanh.MyWebSpringMvc.entities.User;

public interface UserService{
	public List<User> getAll();
	public void addItems(User user);
	public void updateItem(User user);
	public User getItemById(int id);
	public void delete(int id);
}
