package vn.thanh.MyWebSpringMvc.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import vn.thanh.MyWebSpringMvc.entities.User;

@Repository
public class UserDaoImp implements UserDao {
//	private static final Logger logger = LoggerFactory.getLogger(UserDaoImp.class);

	private SessionFactory sessionFactory;

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	// Get All User
	@SuppressWarnings("unchecked")
	@Override
	public List<User> getAll() {
//		Session session = sessionFactory.openSession();
		Session session = sessionFactory.getCurrentSession();
		List<User> listUser = session.createQuery("from User").list();
//		for (User user : listUser) {
//			logger.info("User List::"+user);
//		}
		return listUser;
	}

	// add User
	@Override
	public void addItems(User user) {
		Session sesion = sessionFactory.getCurrentSession();
		sesion.persist(user);
	}

	// update User
	@Override
	public void updateItem(User user) {
		Session session = sessionFactory.getCurrentSession();
		session.update(user);
	}


	@Override
	public User getItemById(int id) {
		Session session = sessionFactory.openSession();
		User user = (User) session.load(User.class, new Integer(id));
		return user;
	}


	@Override
	public void delete(int id) {
		Session session = sessionFactory.getCurrentSession();
		User user = (User) session.get(User.class, id); //load(User.class, new Integer(id));
		if(user != null) {
			session.delete(user);
		}
	}

}
